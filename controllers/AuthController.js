// Como vamos a hacer uso de io, tenemos que importarlo en este archivo .js, referenciándolo ahora con '..'
const io = require('../io');

function loginUserV1(req,res) {
  console.log("POST /apitechu/v1/login");

  console.log(req.body.email);
  console.log(req.body.password);

  var users = require('../usuarios.json');
  var encontrado = false;
  var idUsuario;

   for (var i = 0; i < users.length; i++) {
     console.log("comparando " + users[i].email + " y " +  req.body.email);
     if ((users[i].email == req.body.email) && (users[i].password == req.body.password)){
       console.log("Usuario y contraseña coinciden");
       users[i].logged = true;
       encontrado = true;
       idUsuario = users[i].id;
       break;
     }
   }

   if (encontrado) {
     io.writeUserDataToFile(users);
     //writeUserDataToFile(users);
   }

   var msg = encontrado ? "mensaje : Login correcto , idUsuario : " +  idUsuario : "mensaje : Login incorrecto";

   console.log(msg);
   res.send({"msg" : msg});
}

function logoutUserV1(req,res) {
  console.log("POST /apitechu/v1/logout/:id");

  console.log("id es " + req.params.id);


  var users = require('../usuarios.json');
  var encontrado = false;
  var idUsuario;

   for (var i = 0; i < users.length; i++) {
     console.log("comparando " + users[i].id + " y " +  req.params.id);
     if (users[i].id == req.params.id && users[i].logged == true){
       console.log("El usuario hace Logout");
       delete users[i].logged;
       encontrado = true;
       idUsuario = users[i].id;
       break;
     }
   }

   if (encontrado) {
     io.writeUserDataToFile(users);
     //writeUserDataToFile(users);
   }


   var msg = encontrado ? "mensaje : Logout correcto , idUsuario : " +  idUsuario : "mensaje : Logout incorrecto";

   console.log(msg);
   res.send({"msg" : msg});
}

module.exports.loginUserV1 = loginUserV1;
module.exports.logoutUserV1 = logoutUserV1;
