//nos traemos el framework
const express = require('express');
//por convenio, se llama app
const app = express();

// Importamos las funciones exportadas que se hayan indicado en nuestro fichero io.js (no hay que poner .js en el require)
const io = require('./io');

// Importamos las funciones exportadas que se hayan indicado en nuestro fichero UserController.js
const userController = require('./controllers/UserController');

// Importamos las funciones exportadas que se hayan indicado en nuestro fichero AuthController.js
const authController = require('./controllers/AuthController');

// Con esto pasamos una capa middleware al framework para que pueda parsear los parámetros enviados en el BODY
app.use(express.json());

// si existe la variable de entorno PORT, lo recupera. Si no, recupera el puerto 3000
const port = process.env.PORT || 3000;
app.listen(port);
console.log("API escuchando en el puerto " + port);

// Lista de usuarios
app.get('/apitechu/v1/users', userController.getUsersV1);

// Añadir un nuevo usuario
app.post('/apitechu/v1/users', userController.createUserV1);

// Eliminamos un usuario de la lista
app.delete('/apitechu/v1/users/:id', userController.deleteUserV1);

// Añadir un nuevo usuario
app.post('/apitechu/v1/login', authController.loginUserV1);

// Logout del usuario
app.post('/apitechu/v1/logout/:id', authController.logoutUserV1);


app.get('/apitechu/v1/hello',
  function(req, res) {
      console.log("GET /apitechu/v1/hello");

      //res.send('Hola desde API TechU');
      //res.send('{"msg" : "Hola desde API TechU"}');

      /* Si le quitamos las comillas, directamente Postman devolverá un JSON en lugar de HTML.
       * En el curso tenemos que enviar siempre las respuesas de este modo
       */
      res.send({"msg" : "Hola desde API TechU"});
  }
)

app.post('/apitechu/v1/monstruo/:p1/:p2',
  function (req, res) {
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
  }
)

// Obtener la lista de usuarios Oswaldo
/*app.get('/apitechu/v1/users',
  function(req, res) {
      console.log("GET /apitechu/v1/users");

      // __dirname es el path en el que se está ejecutando, en nuestro caso /home/alumno/Escritorio/projects/practitionerNov2018/borrador_proyecto
      // res.sendFile('usuarios.json', {root:__dirname});

      // se puede hacer de este otro modo, siendo users el array de usuarios porque así se definió la lista de usuarios en mackaroo
      var users = require('./usuarios.json');
      var num_users = users.length;
      console.log("tamanio = " + num_users);

      console.log("req.query.$top = " + req.query.$top);
      var top  = req.query.$top!=undefined ? users.slice(0,req.query.$top):users;


      var tamanio  = req.query.$count == "true" ? num_users:"";
      console.log("tamaño despues = " + tamanio);

      var usersData = {
        "seleccion" : top,
      };

      var usersDataCount = {
        "seleccion" : top,
        "tamaño" : tamanio
      };

      tamanio == "" ? res.send(usersData) : res.send(usersDataCount);
  }
)
*/
