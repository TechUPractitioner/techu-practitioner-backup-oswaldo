const fs = require('fs');

function writeUserDataToFile(data) {
  console.log("writeUserDataToFile");

  var jsonUserData = JSON.stringify(data);

  // escribimos en el fichero usuarios.json la nueva lista de usuarios.
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err) {
      if (err) {
        console.log(err);
      }else {
        console.log("Usuario persistido");
      }
    }
  )

}

// indicamos que lo sacamos fuera para que se pueda referenciar desde otro archivo.
module.exports.writeUserDataToFile = writeUserDataToFile;
